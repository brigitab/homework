package figure;

//information about the figure shaped L, main shape
public class FigureL extends Shape {

	public FigureL() {

		super(new int[][] { { -1, -1 }, { 0, -1 }, { 0, 0 }, { 0, 1 } }); // coordinates

	}

}
