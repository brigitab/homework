package figure;

import java.awt.Color;
import java.awt.Graphics;

public class Block {

	public static int SIZE = 30; // size, that is used as constant

	int x;
	int y;

	public Block(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void paintMe(Graphics g, int x1, int y1) { // color, offset,
														// coordinates on
		g.setColor(Color.pink);
		g.fillRect(x * SIZE + x1 * SIZE, y * SIZE + y1 * SIZE, SIZE, SIZE);
	}

}
