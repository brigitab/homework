package figure;

import java.awt.Graphics;
import java.util.ArrayList;

public abstract class Shape { // information about the block

	public int x;
	public int y;

	private ArrayList<Block> blocks = new ArrayList<>();

	public Shape(int[][] ShapeCoordinate) { // shape class constructor

		for (int[] coords : ShapeCoordinate) {
			blocks.add(new Block(coords[0], coords[1]));
		}

	}

	public void paintItself(Graphics g) {

		for (Block block : blocks) {
			block.paintMe(g, x, y);
		}
	}

	// }

}
