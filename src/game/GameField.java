package game;

import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

import figure.FigureL;
import figure.Shape;

public class GameField extends JPanel {

	/**
	 * This is the main class Simplified Tetris Author: Brigita Brjuhhanov
	 */
	private static final long serialVersionUID = 1487972170649005508L;
	public Shape shape = new FigureL(); // used figure

	// Array for fallen figures
	public ArrayList<Shape> fallenArrayList = new ArrayList<>();

	public GameField(int[][] FallenCoordinate) {
	}

	public GameField() {

		shape.x = 4;
		shape.y = 5;
	}
	// Game size

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(700, 700);
	}

	// Drawing fallen figures
	@Override
	protected void paintComponent(Graphics g) {

		shape.paintItself(g);
		for (Shape fallen : fallenArrayList) {
			fallen.paintItself(g);
		}
		repaint();

	}

}
