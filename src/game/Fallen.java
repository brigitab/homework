package game;

//Class for fallen pieces ArrayList
import java.awt.Color;
import java.awt.Graphics;

public class Fallen {
	public int x;
	public int y;

	public Fallen(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void paintMe(Graphics g, int x, int y) {
		g.setColor(Color.pink);
		g.fillRect(x + x, y + y, y, y);

	}

}
