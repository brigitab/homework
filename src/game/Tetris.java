package game;

//Gamefield, keys, falling
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.Timer;

import figure.FigureL;

public class Tetris {

	public static void main(String[] args) {

		GameField field = new GameField(); // GameField's options

		JFrame window = new JFrame();
		window.setContentPane(field);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.pack();
		window.setVisible(true);
		window.setTitle("Simplified Tetris (Author: Brigita Brjuhhanov, IA18)"); //Author and title

		// Keys, that will make figure move (left, right, down)

		window.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				int key = e.getKeyCode();
				switch (key) {
				case KeyEvent.VK_LEFT: // Left arrow
				case KeyEvent.VK_KP_LEFT: // Keyboards with numpad
					field.shape.x = field.shape.x - 1;
					break;
				case KeyEvent.VK_RIGHT: // Right arrow
				case KeyEvent.VK_KP_RIGHT:
					field.shape.x = field.shape.x + 1;
					break;
				case KeyEvent.VK_DOWN: // Down arrow
				case KeyEvent.VK_KP_DOWN:
					field.shape.y = field.shape.y + 1;
				}

			}
		});

		// method, that will make figure fall

		new Timer(1000, new ActionListener() { //falling speed 

			@Override
			public void actionPerformed(ActionEvent e) {
				field.shape.y = field.shape.y + 1;
				if (field.shape.y > 17) {
					field.fallenArrayList.add(field.shape);
					field.shape = new FigureL();
					field.shape.x = field.shape.x + 7;
					field.shape.y = field.shape.y + 3;
				}
			}
		}).start();
		;

	}

}
